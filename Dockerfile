# This file is a template, and might need editing before it works on your project.
FROM ruby:2.5

ADD ./ /APP/
# Edit with nodejs, mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.

WORKDIR /app

ENV PORT 3000

# For Rails
EXPOSE 3000

CMD ["sh", "-C", "while", :; do ruby ./server.rb; done"]
